import React, { useEffect, useState } from "react";


export default function Home() {
    const [persons, setPersons] = useState<any[]>([]);
    const [selected, setSelected] = useState<any>(null);

    useEffect(() => {
        fetch('http://localhost:4000/person')
            .then(response => response.json())
            .then(data => setPersons(data));
    }, []);

    
    return (
        <section>
            <h1>Persons List</h1>
            
            <div className="list-group">
                {persons.map(person => 
                    <button key={person.id} onClick={() => setSelected(person)} className="list-group-item">{person.name} {person.firstname}</button>
                )}
            </div>
            
            {selected && 
            <div className="modal" tabIndex={-1} role="dialog" style={{display:'block'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{selected.name}</h5>
                        <button type="button" className="btn-close" onClick={() => setSelected(null)} >
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Firstname : {selected.firstname}</p>
                        
                        <p>Age : {selected.age===-1? 'deceded': selected.age} </p>
                    </div>
                    </div>
                </div>
            </div>
            }
        </section>
    )
}
