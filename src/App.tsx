import React, { useState } from 'react';

import './App.css';
import Home from './pages/Home';
import AddPerson from './pages/AddPerson';
import { HeaderHome } from './components/HeaderHome';

function App() {
  const [page, setPage] = useState('Home');

  const displayPage = () => {
    if(page === 'Home') {
      return <Home />;
    }
    if(page === 'Add Person') {
      return <AddPerson />
    }

  }

  return (
    <div className="App">
      <HeaderHome/>

      <div className="container-fluid">
        {displayPage()}
      </div>

    </div>
  );
}

export default App;
